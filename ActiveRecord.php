<?php
/**
 * @author Kyrychenko Ievgenii <shadow_exe@ukr.net>
 * @created 27.03.2015
 * @copyright .rathil
 */

namespace rathil\yii2\mongodb;

use rathil\yii2\mongodb\interfaces\IParentModel;
use rathil\yii2\mongodb\traits\DocAttributesModel;
use yii\helpers\ArrayHelper;
use yii\mongodb\ActiveRecord as YiiActiveRecord;

/**
 * Class ActiveRecord
 * @package rathil\yii2\mongodb
 *
 * @property \MongoId $_id
 */
abstract class ActiveRecord extends YiiActiveRecord implements IParentModel
{
	use DocAttributesModel;

	public function getDirtyAttributes($names = null)
	{
		return ArrayHelper::toArray(
			parent::getDirtyAttributes($names)
		);
	}

	public static function populateRecord($record, $row)
	{
		$record->setAttributes($row, false);
		$record->setOldAttributes($row);
	}
}