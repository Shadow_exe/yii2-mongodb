<?php
/**
 * @author Kyrychenko Ievgenii <shadow_exe@ukr.net>
 * @created 26.03.2015
 * @copyright .rathil
 */

namespace rathil\yii2\mongodb\validators;

use Yii;
use yii\validators\Validator;

/**
 * Validator for checking instances type.
 * Class InstanceOfValidator
 * @package rathil\yii2\mongodb\validators
 */
class InstanceOfValidator extends Validator
{
	/**
	 * The full name of the class. Verifiable the variable must be an instance of this class.
	 * @var string
	 */
	public $instanceOf = '';

	public function isEmpty($value)
	{
		return $value === null;
	}

	protected function validateValue($value)
	{
		if ($value instanceof $this->instanceOf) {
			return null;
		}
		return [Yii::t('yii', '{attribute} must be instance of "{type}".'), [
			'type' => $this->instanceOf
		]]; // TODO yii => rathil
	}
}