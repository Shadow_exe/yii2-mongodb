<?php
/**
 * @author Kyrychenko Ievgenii <shadow_exe@ukr.net>
 * @created 27.03.2015
 * @copyright .rathil
 */

namespace rathil\yii2\mongodb\validators;

use Yii;
use yii\validators\Validator;

/**
 * Validator for checking of arrays.
 * Class ArrayOfMixedValidator
 * @package rathil\yii2\mongodb\validators
 */
class ArrayOfMixedValidator extends Validator
{
	/**
	 * The full name of the internal (original) validator.
	 * @var string
	 */
	public $validator;
	/**
	 * Parameters for internal (original) validator.
	 * @var array
	 */
	public $params = [];
	/**
	 * @var \yii\validators\Validator
	 */
	private $internalValidator;

	/**
	 * Create internal (original) validator for verify variable.
	 * @return Validator
	 */
	protected function getInternalValidator()
	{
		if ($this->internalValidator === null) {
			$params = $this->params;
			if (isset(static::$builtInValidators[$this->validator])) {
				$validator = static::$builtInValidators[$this->validator];
			} else {
				$validator = $this->validator;
			}
			if (is_array($validator)) {
				$params = array_merge($validator, $params);
			} else {
				$params['class'] = $validator;
			}
			$this->internalValidator = Yii::createObject($params);
		}
		return $this->internalValidator;
	}

	/**
	 * Verify by internal (original) validator.
	 * @param mixed $value
	 * @return array|null
	 * @throws \yii\base\NotSupportedException
	 */
	protected function internalValidateValue($value)
	{
		return $this->getInternalValidator()->validateValue($value);
	}

	public function isEmpty($value)
	{
		return $value === null;
	}

	public function validateAttribute($model, $attribute)
	{
		$values = $model->$attribute;
		if (!is_array($values)) {
			$this->addError($model, $attribute, Yii::t('yii', '{attribute} must be array.'), []); // TODO yii => rathil
			return;
		}
		foreach ($values as $value) {
			$result = $this->internalValidateValue($value);
			if (!empty($result)) {
				$this->addError($model, $attribute, $result[0], $result[1]);
			}
		}
	}

	protected function validateValue($values)
	{
		if (!is_array($values)) {
			return [Yii::t('yii', '{attribute} must be array.'), []]; // TODO yii => rathil
		}
		foreach ($values as $value) {
			$result = $this->internalValidateValue($value);
			if (!empty($result)) {
				return $result;
			}
		}
		return null;
	}
}