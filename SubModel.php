<?php
/**
 * @author Kyrychenko Ievgenii <shadow_exe@ukr.net>
 * @created 05.04.2015
 * @copyright .rathil
 */

namespace rathil\yii2\mongodb;

use rathil\yii2\mongodb\interfaces\IParentModel;
use rathil\yii2\mongodb\interfaces\ISubModel;
use rathil\yii2\mongodb\traits\DocAttributesModel;
use yii\base\Model;

abstract class SubModel extends Model implements ISubModel
{
	use DocAttributesModel;

	/**
	 * SubModel`s data.
	 * @var array
	 */
	private $data = [];

	protected function isExistsAttribute($name)
	{
		return isset(static::docAttributes()[$name]) || array_key_exists($name, static::docAttributes());
	}

	public function __set($name, $value)
	{
		if ($this->isExistsAttribute($name)) {
			$this->data[$name] = $this->typecastOnSet($name, $value);
			return;
		}
		parent::__set($name, $value);
	}

	public function __get($name)
	{
		if ($this->isExistsAttribute($name)) {
			return $this->typecastOnGetBase(
				$name,
				isset($this->data[$name]) ? $this->data[$name] : null
			);
		}
		return parent::__get($name);
	}

	/**
	 * @var IParentModel
	 */
	private $parent;

	/**
	 * Get parent model.
	 * @return IParentModel
	 */
	public function getParent()
	{
		return $this->parent;
	}

	/**
	 * Set parent model for sub-model.
	 * @param IParentModel $parent
	 * @return void
	 */
	public function setParent(IParentModel $parent)
	{
		$this->parent = $parent;
	}

	/**
	 * Set attributes data to sub-model.
	 * @param array|object $data
	 * @return void
	 */
	public function setSubModelData($data)
	{
		$this->setAttributes($data, false);
	}
}