<?php
/**
 * @author Kyrychenko Ievgenii <shadow_exe@ukr.net>
 * @created 28.03.2015
 * @copyright .rathil
 */

namespace rathil\yii2\mongodb\traits;

use ArrayObject;
use rathil\yii2\mongodb\interfaces\IDocAttributes;
use rathil\yii2\mongodb\validators\ArrayOfMixedValidator;
use yii\base\Model;
use yii\base\ModelEvent;
use yii\validators\Validator;

/**
 * Trait for get the attributes from scheme (doc block) of yii base model classes.
 * <code>
 * @ property <type> $<name>
 * </code>
 * <b>Use:</b><br/>
 * ~~~~~~~~~~~~~~
 * <code>
 * namespace my\name\space;
 *
 * use yii\base\Model;
 * use rathil\yii2\mongodb\interfaces\ISubModel;
 * use rathil\yii2\mongodb\traits\DocAttributesModel;
 *
 * class MyModel extends Model implements ISubModel {
 *     use DocAttributesModel;
 *     public function attributes()
 *     {
 *         return array_keys(static::docAttributes());
 *     }
 *     // Your implementation of its model
 * }
 * </code>
 * ~~~~~~~~~~~~~~<br/>
 * <b>or</b><br/>
 * ~~~~~~~~~~~~~~<br/>
 * <code>
 * namespace my\name\space;
 *
 * use my\name\space2\MyParentModel;
 * use rathil\yii2\mongodb\interfaces\ISubModel;
 * use rathil\yii2\mongodb\traits\DocAttributesModel;
 *
 * class MyModel extends MyParentModel implements ISubModel {
 *     use DocAttributesModel;
 *     public function attributes()
 *     {
 *         return array_merge(parent::attributes(), array_keys(static::docAttributes()));
 *     }
 *     // Your implementation of its model
 * }
 * </code>
 * ~~~~~~~~~~~~~~<br/>
 * <b>or</b><br/>
 * ~~~~~~~~~~~~~~<br/>
 * <code>
 * namespace my\name\space;
 *
 * use yii\base\Model;
 * use rathil\yii2\mongodb\interfaces\ISubModel;
 * use rathil\yii2\mongodb\traits\DocAttributesModel;
 *
 * abstract class MyParentModel extends Model implements ISubModel {
 *     use DocAttributesModel;
 *     public function attributes()
 *     {
 *         return array_keys(static::docAttributes());
 *     }
 *     // Your implementation of its parent model
 * }
 * class MyModel extends MyParentModel {
 *     // Your implementation of its model
 * }
 * </code>
 * ~~~~~~~~~~~~~~
 * @package rathil\yii2\mongodb\traits
 */
trait DocAttributesModel
{
	use DocAttributes {
		typecastClass as typecastClassBase;
		typecastOnGet as typecastOnGetBase;
	}

	/**
	 * Generate validation rules by attribute or not.
	 * @return bool
	 */
	protected function isCreateAttributeRules()
	{
		return true;
	}

	/**
	 * Create validator by description attribute.
	 * @param array $attributeInfo
	 * @param array $params
	 * @return Validator
	 */
	protected function createValidatorInternal(array $attributeInfo, array $params)
	{
		if ($attributeInfo['isArray']) {
			return Validator::createValidator(
				ArrayOfMixedValidator::className(), $this, [], [
				'validator' => $attributeInfo['validator'],
				'params' => $params,
			]);
		}
		return Validator::createValidator(
			$attributeInfo['validator'], $this, [], $params
		);
	}

	/**
	 * Creating a handler in sub-model on validation parent model.
	 * @param Model $model
	 * @param string $key
	 */
	protected function linkValidation(Model $model, $key)
	{
		/** @var Model $this */
		$this->on(Model::EVENT_BEFORE_VALIDATE, function (ModelEvent $event) use ($model, $key) {
			if ($model->validate()) {
				return;
			}
			foreach ($model->getErrors() as $errorKey => $error) {
				$event->sender->addError(
					$key . '.' . $errorKey,
					$error
				);
			}
		});
	}

	protected function typecastOnGet($name, $value)
	{
		$result = $this->typecastOnGetBase($name, $value);
		if ($result !== $value) {
			parent::__set($name, $result);
		}
		return $result;
	}

	protected function typecastClass($name, &$value)
	{
		$result = $this->typecastClassBase($name, $value);
		if ($result) {
			if (is_array($value)) {
				foreach ($value as $key => $model) {
					if ($model instanceof Model) {
						$this->linkValidation(
							$model,
							$name . '[' . $key . ']'
						);
					}
				}
			} elseif ($value instanceof Model) {
				$this->linkValidation(
					$value,
					$name
				);
			}
		}
		return $result;
	}

	public function createValidators()
	{
		/** @var ArrayObject $result */
		$result = parent::createValidators();
		if ($this->isCreateAttributeRules()) {
			$validators = [];
			foreach (static::docAttributes() as $attributeName => $attributeInfo) {
				if ($attributeInfo['typecast'] == IDocAttributes::TYPECAST_SCALAR) {
					$validatorId = sprintf('%s-%d', $attributeInfo['validator'], $attributeInfo['isArray']);
					$params = [];
				} elseif ($attributeInfo['typecast'] == IDocAttributes::TYPECAST_CLASS) {
					$validatorId = sprintf('%s-%d', $attributeInfo['type'], $attributeInfo['isArray']);
					$params = [
						'instanceOf' => $attributeInfo['type'],
					];
				} else {
					continue;
				}
				if (empty($validators[$validatorId])) {
					$validators[$validatorId] = $this->createValidatorInternal($attributeInfo, $params);
				}
				$validators[$validatorId]->attributes[] = $attributeName;
			}
			foreach ($validators as $validator) {
				$result->append($validator);
			}
		}
		return $result;
	}

	public function __set($name, $value)
	{
		parent::__set(
			$name,
			$this->typecastOnSet($name, $value)
		);
	}

	public function __get($name)
	{
		return $this->typecastOnGet(
			$name,
			parent::__get($name)
		);
	}

	public function attributes()
	{
		return array_keys(static::docAttributes());
	}
}