<?php
/**
 * @author Kyrychenko Ievgenii <shadow_exe@ukr.net>
 * @created 27.03.2015
 * @copyright .rathil
 */

namespace rathil\yii2\mongodb\traits;

use rathil\yii2\mongodb\interfaces\IDocAttributes;
use rathil\yii2\mongodb\validators\InstanceOfValidator;
use rathil\yii2\mongodb\interfaces\ISubModel;
use ReflectionClass;
use Yii;
use yii\caching\Cache;

/**
 * Trait for get the attributes from scheme (doc block) of custom model classes.
 * <code>
 * @ property <type> $<name>
 * </code>
 * <b>Use:</b><br/>
 * ~~~~~~~~~~~~~~
 * <code>
 * namespace my\name\space;
 *
 * use my\name\space2\MyModelNotChildOfYiiBaseModel;
 * use rathil\yii2\mongodb\interfaces\ISubModel;
 * use rathil\yii2\mongodb\traits\DocAttributes;
 *
 * class MyModel extends MyModelNotChildOfYiiBaseModel implements ISubModel {
 *     use DocAttributes;
 *     // Your implementation of its model
 * }
 * </code>
 * ~~~~~~~~~~~~~~<br/>
 * <b>or</b><br/>
 * ~~~~~~~~~~~~~~<br/>
 * <code>
 * namespace my\name\space;
 *
 * use my\name\space2\MyParentModel;
 * use rathil\yii2\mongodb\interfaces\ISubModel;
 * use rathil\yii2\mongodb\traits\DocAttributes;
 *
 * class MyModel extends MyParentModel implements ISubModel {
 *     use DocAttributes;
 *     // Your implementation of its model
 * }
 * </code>
 * ~~~~~~~~~~~~~~<br/>
 * <b>or</b><br/>
 * ~~~~~~~~~~~~~~<br/>
 * <code>
 * namespace my\name\space;
 *
 * use my\name\space2\MyModelNotChildOfYiiBaseModel;
 * use rathil\yii2\mongodb\interfaces\ISubModel;
 * use rathil\yii2\mongodb\traits\DocAttributes;
 *
 * abstract class MyParentModel extends MyModelNotChildOfYiiBaseModel implements ISubModel {
 *     use DocAttributes;
 *     // Your implementation of its parent model
 * }
 * class MyModel extends MyParentModel {
 *     // Your implementation of its model
 * }
 * </code>
 * ~~~~~~~~~~~~~~
 * @package rathil\yii2\mongodb\traits
 */
trait DocAttributes
{
	/**
	 * Already converted to type attributes.
	 * @var string[]
	 */
	private $typecastAlready = [];

	/**
	 * All schemes of attributes by class name.
	 * @var array
	 */
	private static $attributes = [];
	/**
	 * All namespaces of classes.
	 * @var array
	 */
	private static $namespaces = [];

	/**
	 * Cache information about the attributes or not.
	 * @return bool
	 */
	protected static function isCacheDocAttribute()
	{
		return YII_ENV_PROD;
	}

	/**
	 * Typecast the attribute type to specified in describing the scheme or not.
	 * @return bool
	 */
	protected function isTypecastAttribute()
	{
		return true;
	}

	/**
	 * Object ID of cache components.
	 * @return string
	 */
	protected static function schemaCache()
	{
		return 'cache';
	}

	/**
	 * Creating a schema describing the attribute with scalar type.
	 * @param string $type
	 * @param array $result
	 * @return bool
	 */
	protected static function getAttributeScalarType($type, array &$result)
	{
		$type = strtolower($type);
		switch ($type) {
			case 'int':
			case 'integer':
				$result['type'] = 'integer';
				$result['validator'] = 'integer';
				$result['typecast'] = IDocAttributes::TYPECAST_SCALAR;
				return true;
			case 'double':
			case 'real':
			case 'float':
				$result['type'] = 'float';
				$result['validator'] = 'number';
				$result['typecast'] = IDocAttributes::TYPECAST_SCALAR;
				return true;
			case 'bool':
			case 'boolean':
				$result['type'] = 'boolean';
				$result['validator'] = 'boolean';
				$result['typecast'] = IDocAttributes::TYPECAST_SCALAR;
				return true;
			case 'string':
				$result['type'] = 'string';
				$result['validator'] = 'string';
				$result['typecast'] = IDocAttributes::TYPECAST_SCALAR;
				return true;
		}
		return false;
	}

	/**
	 * Creating a schema describing the attribute with full name of the class.
	 * @param string $type
	 * @param array $result
	 * @return bool
	 */
	protected static function getAttributeFullClassType($type, array &$result)
	{
		if (!class_exists($type)) {
			return false;
		}
		$reflection = new ReflectionClass($type);
		if (!$reflection->isInternal() && !$reflection->implementsInterface('\rathil\yii2\mongodb\interfaces\ISubModel')) {
			return false;
		}
		$result['type'] = $type;
		$result['validator'] = InstanceOfValidator::className();
		$result['typecast'] = IDocAttributes::TYPECAST_CLASS;
		return true;
	}

	/**
	 * Creating a schema describing the attribute with simplistic name of the class.
	 * @param string $type
	 * @param array $result
	 * @param ReflectionClass $reflection
	 * @return bool
	 */
	protected static function getAttributeSimplisticClassType($type, array &$result, ReflectionClass $reflection)
	{
		return static::getAttributeFullClassType('\\' . $reflection->getNamespaceName() . '\\' . $type, $result);
	}

	/**
	 * Creating a schema describing the attribute with small name of the class.
	 * @param string $type
	 * @param array $result
	 * @param ReflectionClass $reflection
	 * @return bool
	 */
	protected static function getAttributeSmallClassType($type, array &$result, ReflectionClass $reflection)
	{
		$class = get_called_class();
		if (!isset(self::$namespaces[$class])) {
			self::$namespaces[$class] = [];
			$namespaces = &self::$namespaces[$class];
			$tokens = token_get_all(file_get_contents($reflection->getFileName()));
			if ($reflection->inNamespace()) {
				$imports = static::findImportsWithNamespace($tokens, $reflection);
			} else {
				$imports = static::findImportsWithoutNamespace($tokens, $reflection);
			}
			if (empty($imports)) {
				return false;
			}
			$pattern = '/^(.*?)\s+as\s+(\w+)|(.*?\\\?(\w+))$/i';
			foreach ($imports as $import) {
				if (preg_match($pattern, $import, $matches)) {
					if (isset($matches[4])) {
						$namespaces[$matches[4]] = '\\' . trim($matches[3], '\\');
						continue;
					}
					$namespaces[$matches[2]] = '\\' . trim($matches[1], '\\');
				}
			}
		}
		if (isset(self::$namespaces[$class][$type])) {
			return static::getAttributeFullClassType(self::$namespaces[$class][$type], $result);
		}
		return false;
	}

	/**
	 * Find all imported namespaces in file with namespaces.
	 * @param array $tokens
	 * @param ReflectionClass $reflection
	 * @return array
	 */
	protected static function findImportsWithNamespace(array &$tokens, ReflectionClass $reflection)
	{
		$imports = [];
		do {
			$token = current($tokens);
			if (is_array($token) && $token[0] == T_NAMESPACE) {
				$namespace = '';
				while ($token = next($tokens)) {
					if (in_array($token, ['{', ';'])) break;
					$namespace .= $token[1];
				}
				$namespace = trim($namespace);
				if ($namespace != $reflection->getNamespaceName()) {
					continue;
				}
				if (static::findImports($imports, $tokens)) {
					break;
				}
			}
		} while (next($tokens));
		return $imports;
	}

	/**
	 * Find all imported namespaces in file without namespaces.
	 * @param array $tokens
	 * @param ReflectionClass $reflection
	 * @return array
	 */
	protected static function findImportsWithoutNamespace(array &$tokens, ReflectionClass $reflection)
	{
		$imports = [];
		static::findImports($imports, $tokens);
		return $imports;
	}

	/**
	 * Find all imported namespaces in file.
	 * @param array $imports
	 * @param array $tokens
	 * @return bool
	 */
	protected static function findImports(array &$imports, array &$tokens)
	{
		$bracesCount = 0;
		while ($token = next($tokens)) {
			if ($token == '}') {
				if ($bracesCount == 0) {
					return true;
				}
				--$bracesCount;
				continue;
			}
			if ($token == '{') {
				++$bracesCount;
				continue;
			}
			if ($bracesCount > 0) {
				continue;
			}
			if (is_array($token) && $token[0] == T_USE) {
				$import = '';
				while ($token = next($tokens)) {
					if ($token == ';') break;
					if (!is_array($token)) {
						$import .= $token;
						continue;
					}
					$import .= $token[1];
				}
				$imports = array_merge(
					$imports,
					preg_split('/\s*,\s*/im', trim($import), -1, PREG_SPLIT_NO_EMPTY)
				);
			}
		}
		return false;
	}

	/**
	 * Get a schema describing the attribute.
	 * @param string $type
	 * @param ReflectionClass $reflection
	 * @return array
	 */
	protected static function attributesType($type, ReflectionClass $reflection)
	{
		if (substr($type, -2) == '[]') {
			$result = static::attributesType(substr($type, 0, -2), $reflection);
			$result['isArray'] = true;
			return $result;
		}
		$result = [
			'isArray' => false,
			'type' => 'mixed',
			'validator' => null,
			'typecast' => IDocAttributes::TYPECAST_NONE,
		];
		if (empty($type)) {
			return $result;
		}

		static::getAttributeScalarType($type, $result)
		|| static::getAttributeFullClassType($type, $result)
		|| static::getAttributeSimplisticClassType($type, $result, $reflection)
		|| static::getAttributeSmallClassType($type, $result, $reflection);

		return $result;
	}

	/**
	 * Get all the attributes of the class and parent classes attributes with their schemes.
	 * <code>
	 * [
	 *     '_id' => [
	 *         'isArray' => false,
	 *         'type' => '\MongoId',
	 *         'validator' => '\rathil\mongodb\validators\InstanceOfValidator',
	 *         'typecast' => IDocAttributes::TYPECAST_CLASS
	 *     ],
	 *     ...
	 * ];
	 * </code>
	 *
	 * @see \rathil\mongodb\interfaces\IDocAttributes
	 * @see \rathil\mongodb\validators\InstanceOfValidator
	 * @see \rathil\mongodb\validators\ArrayOfMixedValidator
	 *
	 * @return array
	 */
	public static function docAttributes()
	{
		$class = get_called_class();
		if (!isset(self::$attributes[$class])) {
			self::$attributes[$class] = [];
			$attributes = &self::$attributes[$class];
			$cache = null;
			if (static::isCacheDocAttribute()) {
				$cache = Yii::$app->get(static::schemaCache(), false);
				if ($cache instanceof Cache && ($attributes = $cache->get($class)) !== false) {
					return $attributes;
				}
			}
			$attributes = [];
			$reflection = new ReflectionClass($class);
			if ($class != __CLASS__) {
				$attributes = call_user_func([$reflection->getParentClass()->getName(), __FUNCTION__]);
			}
			$doc = $reflection->getDocComment();
			if ($doc !== false) {
				$pattern = '/\*\s*@property\s+([^\s\$\|]*)\s*(\S+)/im';
				if (preg_match_all($pattern, $doc, $matches, PREG_SET_ORDER) !== false) {
					foreach ($matches as $match) {
						$attributes[trim($match[2], '$')] = static::attributesType($match[1], $reflection);
					}
				}
			}
			if (static::isCacheDocAttribute() && $cache instanceof Cache) {
				$cache->set($class, $attributes);
			}
		}
		return self::$attributes[$class];
	}

	/**
	 * Typecast the attribute type on <b>SET</b> attribute.
	 * @param string $name
	 * @param mixed $value
	 * @return mixed
	 */
	protected function typecastOnSet($name, $value)
	{
		if (!$this->isCanTypecast($name)) {
			return $value;
		}

		return $this->typecast($name, $value);
	}

	/**
	 * Typecast the attribute type on <b>GET</b> attribute.
	 * @param string $name
	 * @param mixed $value
	 * @return mixed
	 */
	protected function typecastOnGet($name, $value)
	{
		if ($value === null || !$this->isCanTypecast($name) || isset($this->typecastAlready[$name])) {
			return $value;
		}

		return $this->typecast($name, $value);
	}

	/**
	 * Typecast the attribute type.
	 * @param string $name
	 * @param mixed $value
	 * @return mixed
	 */
	protected function typecast($name, $value)
	{
		$this->typecastAlready[$name] = true;

		$this->typecastScalar($name, $value)
		|| $this->typecastClass($name, $value);

		return $value;
	}

	/**
	 * Typecast scalar attribute.
	 * @param string $name
	 * @param mixed $value
	 * @return bool
	 */
	protected function typecastScalar($name, &$value)
	{
		$attribute = static::docAttributes()[$name];
		if ($attribute['typecast'] != IDocAttributes::TYPECAST_SCALAR) {
			return false;
		}
		if (!$attribute['isArray']) {
			settype($value, $attribute['type']);
			return true;
		}
		settype($value, 'array');
		foreach ($value as &$valueOne) {
			settype($valueOne, $attribute['type']);
		}
		return true;
	}

	/**
	 * Typecast attribute to class.
	 * @param string $name
	 * @param mixed $value
	 * @return bool
	 */
	protected function typecastClass($name, &$value)
	{
		$attribute = static::docAttributes()[$name];
		if ($attribute['typecast'] != IDocAttributes::TYPECAST_CLASS) {
			return false;
		}
		if (!$attribute['isArray']) {
			$value = $this->typecastClassCreateObject($attribute, $value);
			return true;
		}
		if (!is_array($value)) {
			$value = [$this->typecastClassCreateObject($attribute, $value)];
			return true;
		}
		$newValue = [];
		foreach ($value as $valueOne) {
			$newValue[] = $this->typecastClassCreateObject($attribute, $valueOne);
		}
		$value = $newValue;
		return true;
	}

	/**
	 * Create sub-model for Model.
	 * @param array $attribute
	 * @param mixed $value
	 * @return ISubModel
	 */
	protected function typecastClassCreateObject(array $attribute, $value)
	{
		if ($value instanceof $attribute['type']) {
			if ($value instanceof ISubModel) {
				$value->setParent($this);
			}
			return $value;
		}
		/** @var ISubModel $newValue */
		$newValue = new $attribute['type'];
		if ($newValue instanceof ISubModel) {
			$newValue->setParent($this);
		}
		if (is_array($value)) {
			$newValue->setSubModelData($value);
		}
		return $newValue;
	}

	/**
	 * Verify capability typecast of a particular attribute.
	 * @param string $name
	 * @return bool
	 */
	protected function isCanTypecast($name)
	{
		return $this->isTypecastAttribute() && isset(static::docAttributes()[$name]);
	}
}