<?php
/**
 * @author Kyrychenko Ievgenii <shadow_exe@ukr.net>
 * @created 29.03.2015
 * @copyright .rathil
 */

namespace rathil\yii2\mongodb\interfaces;

/**
 * Base interface for sub-model.
 * Interface ISubModel
 * @package rathil\yii2\mongodb\interfaces
 */
interface ISubModel extends IParentModel
{
	/**
	 * Set parent model for sub-model.
	 * @param IParentModel $parent
	 * @return void
	 */
	public function setParent(IParentModel $parent);

	/**
	 * Set attributes data to sub-model.
	 * @param array|object $data
	 * @return void
	 */
	public function setSubModelData($data);
}