<?php
/**
 * @author Kyrychenko Ievgenii <shadow_exe@ukr.net>
 * @created 05.04.2015
 * @copyright .rathil
 */

namespace rathil\yii2\mongodb\interfaces;

/**
 * Base interface for parent model.
 * Interface IParentModel
 * @package rathil\yii2\mongodb\interfaces
 */
interface IParentModel
{
}