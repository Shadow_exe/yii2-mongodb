<?php
/**
 * @author Kyrychenko Ievgenii <shadow_exe@ukr.net>
 * @created 27.03.2015
 * @copyright .rathil
 */

namespace rathil\yii2\mongodb\interfaces;

/**
 * The list of constants describing the types of attributes.
 * Interface IDocAttributes
 * @package rathil\yii2\mongodb\interfaces
 */
interface IDocAttributes
{
	CONST TYPECAST_NONE = 0;
	CONST TYPECAST_SCALAR = 1;
	CONST TYPECAST_CLASS = 2;
}